#!/usr/bin/env python3

import configparser
import soco
import sys
import argparse


def load_service_cfg():
	config = configparser.ConfigParser()
	config.read('tunein.ini')
	return {
		'service': config['DEFAULT']['Service']
	}


def load_radio_cfg():
	config = configparser.ConfigParser()
	config.read('radio.ini')
	player_name= config['DEFAULT']['PlayerName']

	return {
		'player_name': player_name
	}

def find_player(name):
	devices = soco.discover()
	for device in devices:
		if device.player_name == name:
			return device

	print("No player found with name {}. Found {}"
		.format(player_name, [x.player_name for x in devices]))

	sys.exit()

def find_station(player, station_title):
	results = player.get_favorite_radio_stations(0, 100)
	stations = results['favorites']

	for station in stations:
		if station['title'] == station_title:
			return station

	print("No station found in favourties with title {}. Found {}"
			.format(station_title, [x['title'] for x in stations]))

	sys.exit()

def play_station(player, station, service_config):
	metadata_template = """
	<DIDL-Lite xmlns:dc="http://purl.org/dc/elements/1.1/"
	    xmlns:upnp="urn:schemas-upnp-org:metadata-1-0/upnp/"
	    xmlns:r="urn:schemas-rinconnetworks-com:metadata-1-0/"
	    xmlns="urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/">
	    <item id="R:0/0/0" parentID="R:0/0" restricted="true">
	        <dc:title>{title}</dc:title>
	        <upnp:class>object.item.audioItem.audioBroadcast</upnp:class>
	        <desc id="cdudn" nameSpace="urn:schemas-rinconnetworks-com:metadata-1-0/">
	            {service}
	        </desc>
	    </item>
	</DIDL-Lite>' """
	uri = station['uri']
	uri = uri.replace('&', '&amp;')
	metadata = metadata_template.format(title=station['title'], service=service_config['service'])
	player.play_uri(uri, metadata)



parser = argparse.ArgumentParser(description='Control Radio')
parser.add_argument('station_title', type=str,
                   help='name of station to play')

args = parser.parse_args()
print("station title={}".format(args.station_title))

service_config = load_service_cfg()
radio_config = load_radio_cfg()


player = find_player(radio_config['player_name'])
station = find_station(player, args.station_title)
play_station(player, station, service_config)
